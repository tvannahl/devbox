FROM registry.fedoraproject.org/fedora-toolbox:38

ARG HELM_URL="https://get.helm.sh/helm-v3.7.1-linux-amd64.tar.gz"
ARG HELM_SHA="6cd6cad4b97e10c33c978ff3ac97bb42b68f79766f1d2284cfd62ec04cd177f4"

COPY /etc /etc/

RUN dnf update -y \
	&& dnf install -y \
		ShellCheck \
		ansible \
		bind-utils \
		ctags \
		exa \
		fzf \
		git-lfs \
		htop \
		ipcalc \
		ipcalc \
		iproute \
		iproute \
		jq \
		kubectl \
		mathjax \
		ncdu \
		neovim \
		nmap \
		nodejs \
		pipenv \
		podman-remote \
		psmisc \
		pwgen \
		python-coverage \
		python3-ansible-lint \
		python3-flake8 \
		python3-jedi \
		python3-lxml \
		python3-neovim \
		python3-nltk \
		python3-notebook \
		python3-pandas \
		python3-pydocstyle \
		python3-scikit-image \
		python3-seaborn \
		python3-tox \
		qemu \
		ripgrep \
		sscg \
		telnet \
		virt-install \
		yamllint \
	&& dnf clean all -y

RUN curl -s "$HELM_URL" -o helm.tgz \
	&& DOWNLOAD_SUM=$(sha256sum helm.tgz | awk '{ print $1; }') \
	&& [[ $DOWNLOAD_SUM == "${HELM_SHA}" ]] \
	&& HELM_TEMP="$(mktemp -d)" \
	&& tar xf helm.tgz -C "${HELM_TEMP}" \
	&& mv "${HELM_TEMP}/linux-amd64/helm" /usr/local/bin/ \
	&& rm -rf "${HELM_TEMP}"
